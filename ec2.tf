resource "aws_security_group" "security-group-for-ec2" {
  name        = "ec2-sg"
  description = "Security Group for EC2"
  vpc_id      = aws_vpc.replatform-vpc.id

  tags = {
    "Name" = "ec2-sg"
  }
}

resource "aws_security_group_rule" "allow-http-to-ec2" {
  security_group_id = aws_security_group.security-group-for-ec2.id
  type              = "ingress"
  protocol          = "tcp"
  from_port         = 80
  to_port           = 80
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "allow-any-from-ec2" {
  security_group_id = aws_security_group.security-group-for-ec2.id
  type              = "egress"
  protocol          = "-1"
  from_port         = 0
  to_port           = 0
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_instance" "ec2" {
  instance_type = "t2.micro"
  ami           = "ami-0ffac3e16de16665e"
  subnet_id     = aws_subnet.public-subnet.id
  vpc_security_group_ids = [
    aws_security_group.security-group-for-ec2.id,
  ]
  user_data = <<EOF
  #!/bin/bash
  yum update -y
  yum install -y httpd
  uname -n > /var/www/html/index.html
  systemctl start httpd
  systemctl enable httpd
  EOF

  tags = {
    "Name" = "web-ec2"
  }
}
