resource "aws_vpc" "replatform-vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "replatform-vpc"
  }
}

resource "aws_subnet" "public-subnet" {
  vpc_id                  = aws_vpc.replatform-vpc.id
  cidr_block              = "10.0.0.0/24"
  availability_zone       = "ap-northeast-1a"
  map_public_ip_on_launch = true

  tags = {
    "Name" = "public-subnet-a"
  }
}

resource "aws_internet_gateway" "replatform-igw" {
  vpc_id = aws_vpc.replatform-vpc.id

  tags = {
    "Name" = "replatform-igw"
  }
}

resource "aws_route_table" "replatform-route-table" {
  vpc_id = aws_vpc.replatform-vpc.id

  tags = {
    "Name" = "replatform-route-table"
  }

}

resource "aws_route" "replatform-route" {
  route_table_id         = aws_route_table.replatform-route-table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.replatform-igw.id
}


resource "aws_route_table_association" "route_table_a" {
  route_table_id = aws_route_table.replatform-route-table.id
  subnet_id      = aws_subnet.public-subnet.id
}
