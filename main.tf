terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=4.55"
    }
  }
  required_version = ">=1.0"
}

provider "aws" {
  region = "ap-northeast-1"
}
